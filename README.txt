CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Maintainers


INTRODUCTION
------------
Block Local Tasks turns the standard MENU_LOCAL_TASKS into a block that can be
repositioned or interacted with at the theme layer.  It provides 2 blocks, each
representing a different presentation of the tasks on a page:

1) Plain MENU_LOCAL_TASKS - This is the standard Drupal system output for the
tasks, usually rendered as tabs on the page.

2) Menu MENU_LOCAL_TASKS - This block renders through theme_menu_links and
theme_menu_tree, and provides a nice tree of links similar to Drupal core's
menu system output.  It also allows MENU_LOCAL_TASKS to be integrated with
other administration modules such as the Admin module
(http://drupal.org/project/admin).

Each block exposes several configuration options, including the ability to turn
into an accordion, hiding the task links until a user selects expands the block.


INSTALLATION
------------
Activate the module and assign one of the 2 provided blocks to a theme region.


MAINTAINERS
-----------
- jay.dansand (Jay Dansand)
