(function ($) {
  Drupal.behaviors.block_local_tasks = {
    attach: function (context, settings) {
      var settings = settings.block_local_tasks || {};
      try {
        if (settings.collapsing_block) {
          $('.block-block-local-tasks', context).once('block_local_tasks_collapsing_block', function () {
            var  block = this,
              $block = $(block),
              $handle = $('<div class="block-local-tasks-handle"></div>').appendTo($block),
              $container = $('<div class="block-local-tasks-container"></div>').appendTo($block),
              $block_content = $handle.prevAll(),
              block_title = $block.find('.block-title').text() || Drupal.t('Tasks'),
              $trigger = $('<a href="#" class="block-local-tasks-trigger" />').appendTo($handle);
            $block_content.each(function() {
              $(this).detach().prependTo($container);
            });
            var trigger_text = {
              'show': Drupal.t('Show @block_title', { '@block_title' : block_title }),
              'hide': Drupal.t('Hide @block_title', { '@block_title' : block_title })
            };
            $trigger.text(trigger_text.show);
            $trigger.click(function () {
              $container.toggleClass('block-local-tasks-container-active');
              $block.toggleClass('block-local-tasks-block-active');
              $trigger.toggleClass('block-local-tasks-trigger-active');
              // Flip the state:
              var state = !$(this).data('trigger-active');
              // Update the start_expanded-triggering cookie:
              $.cookie(
                'block_local_tasks_user_state',
                // cookie value ("true"|"false"):
                state.toString(),
                // cookie options:
                {
                  // expiration in days from now (1 year):
                  'expires': 365,
                  'path': '/'
                }
              );
              // Store the status on the trigger element:
              $(this).data('trigger-active', state);
              // Flip the text to reflect the state:
              $trigger.text(state ? trigger_text.hide : trigger_text.show);

              return false;
            });
            if (settings.vanishing_trigger) {
              $block.addClass('block-local-tasks-block-vanishing');
            }
            if (settings.notext_trigger) {
              $trigger.addClass('block-local-tasks-trigger-notext');
            }
            // Determine if the block should be expanded at the outset:
            var expand_block = false;
            if (settings.start_expanded) {
              // If the block should default to expanded, check if the user has
              // collapsed it (stored as a cookie).  Either the cookie doesn't
              // exist, or it's "true"|"false"; if it's unset or true, then
              // we default to expanded:
              if ($.cookie('block_local_tasks_user_state') != 'false') {
                expand_block = true;
              }
            }
            // "Keep Expanded" overrides the "Sart Expanded" default behavior,
            // as it is more specific than an overall strategy:
            if (settings.prevent_collapse_on_tabchange) {
              if ($.cookie('block_local_tasks_keep_expanded') == 'true') {
                expand_block = true;
                // This cookie is evaluated when *the next* URL is loaded, so
                // it has done its job of instructing us to keep the block
                // expanded. Now, we need to prevent future requests from also
                // expanding, unless they are between primary tabs.
                // Clear the cookie (let tabs revert to default behavior):
                $.cookie(
                  'block_local_tasks_keep_expanded',
                  // cookie value (null clears the cookie):
                  null,
                  // cookie options:
                  {
                    // expiration in days from now (negative clears cookie):
                    'expires': -1,
                    'path': '/'
                  }
                );
              }

              $('.tabs.primary a, .menu li:not(.leaf) > a', $block).click(function(e) {
                // Set the cookie (remember to expand block on next page):
                $.cookie(
                  'block_local_tasks_keep_expanded',
                  // cookie value ("true" = expand block on next page):
                  'true',
                  // cookie options:
                  {
                    // expiration in days from now (null = session cookie):
                    'expires': null,
                    'path': '/'
                  }
                );
              });
            }
            // Expand the block by clicking its trigger
            if (expand_block) {
              $trigger.click();
            }
          });
        }
      } catch(e) { }
    },
  }
})(jQuery);
